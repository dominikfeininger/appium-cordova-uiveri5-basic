
exports.config = {
    host: 'localhost', // Use localhost as chrome driver server
    port: 9515, // "9515" is the port opened by chrome driver.
    specs: ["./e2e/**/*.js"],
    framework: 'mocha',
    capabilities: [{
        browserName: 'chrome',
        'goog:chromeOptions': {
            binary: './platforms/electron/build/mac/HelloCordova.app', // Path to your Electron binary.
            args: [/* cli arguments */] // Optional, perhaps 'app=' + /path/to/your/app/
        }
    }]
}