const path = require('path');
let config = require('./wdio_shared.conf').config;

// This defines which kind of device we want to test on, as well as how it should be
// configured.
config.capabilities = [{

  automationName: "UiAutomator2",

  platformName: 'Android',

  // The version of the Android system
  platformVersion: '9',

  // For Android, Appium uses the first device it finds using "adb devices". So, this string simply needs to be non-empty.
  deviceName: 'any',

  // Where to find the .apk or .ipa file to install on the device. The exact location
  // of the file may change depending on your Cordova version.
  app: './platforms/android/app/build/outputs/apk/debug/app-debug.apk',

  // By default, Appium runs tests in the native context. By setting autoWebview to
  // true, it runs our tests in the Cordova context.
  autoWebview: true,

  // When set to true, it will not show permission dialogs, but instead grant all
  // permissions automatically.
  autoGrantPermissions: true
}];

config.wdi5 = {
  screenshotPath: "./tests/report/screenshots",
  test: "test",
  deviceType: "native",
  capabilities: {
    rotate: true,
    camera: 2
  }
};

config.appium = {
  args: ['--allow-insecure', 'chromedriver_autodownload']
};

exports.config = config;
