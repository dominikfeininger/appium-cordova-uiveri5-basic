const path = require('path');
let config = require('./wdio_shared.conf').config;

config.capabilities = [
  {
    automationName: "XCUITest",

    platformName: "iOS",

    // The version of the Android or iOS system
    platformVersion: "13.4",

    // For iOS, this must exactly match the device name as seen in Xcode.
    deviceName: "iPhone 11", // Pro Max

    // Where to find the .apk or .ipa file to install on the device. The exact location
    // of the file may change depending on your Cordova version.
    app: "./platforms/ios/build/emulator/HelloCordova.app",

    // By default, Appium runs tests in the native context. By setting autoWebview to
    // true, it runs our tests in the Cordova context.
    autoWebview: true,

    // When set to true, it will not show permission dialogs, but instead grant all
    // permissions automatically.
    autoGrantPermissions: true
  }
];

config.outputDir = "./tests/report/logs";

config.wdi5 = {
  screenshotPath: "./tests/report/screenshots",
  test: "test",
  deviceType: "native",
  capabilities: {
    rotate: false,
    camera: 1
  }
};

exports.config = config;
