This project is a demo for ui5 tests in a production build cordova app.

Also check this documentation: https://js-soft.atlassian.net/wiki/spaces/~397093474/blog/2020/03/09/1311244349/Testing+of+Webapps+on+Mobile+Devices

# Getting Started (Short)

Precondition is to have a working appium setup with webdriverIO and selenium-standalone
Checkout the appium site: http://appium.io/docs/en/about-appium/getting-started/?lang=de

make sure the `appium-doctor` shows green results at least for your desired platform

# Run it

on JS-Soft nexus package @testing/wdi5 contains the utils and wdio-ui5 shim

## First time setup (Emulator)

### iOs

-   Xcode and Simulator
-   Check Simulator for devices and ios versions
-   appium can launch the simulator itself

### Android

-   Android Studio
-   Install SKD Manager
-   AVD Manager
-   Create Testdevice
-   Start Testdevice (not done by testexecution)

## Start

Execute once
`npm run test_<platform>`where platform can be `ios` or `android`.

With watch
`npm run test_<platform>_w`where platform can be `ios` or `android`.

# Configuration

## Platform

Check the `wdio_<platform>.conf.js`file to fit your local dev environment.

## Appium Configuration

Check folder `/tests` contains three `*.conf.js`files. The shared file contains the chared main configuration for android and ios. Concatinated in the specific platform config file which gets run by the tasks.

# Tests

Folder `/tests/spec/` contains the tests. Nummeration inducates execution order.

# Tech Stuff

## Lifecycle
https://user-images.githubusercontent.com/5413013/58424426-a3522f00-8097-11e9-95d5-26aa109dd03e.png

## Sources

-   Appium
-   WebdriverIO
-   Jasmine
-   UI5
-   Cordova

## What has been done

1. Create cordova app from scratch
2. Integrate openui5 showcase
3. Appium environment setup
4. Write tests

### Structure

The `/tests`folder contains

-   logs -> test error logs in case something went wrong in environment setup
-   report -> test results e.g. screenshots
-   skip -> test which will be skipped, handy if you struggle with a specific testfile to skipp other tests
-   spec -> contains the tests
-   wdio-ui5 -> contains utils and shim/ brigde to ui5

## Execute

#### Android

#### iOs

#### Browser

start a static server with content of www folder ```npm run test

## The tricky parts

### Setup

Package.json
Webdriver Version
git push

#### UI5 custom stuff

can be found in `tests/wdio-ui5`

1.  Functions to execute code in webapp context. This means it switches context from the test into the application.
    `browser.execute`
    Inject a snippet of JavaScript into the page for execution in the context of the currently selected frame. The executed script is assumed to be synchronous and the result of evaluating the script is returned to the client.

        ```browser.executeAsync```
        Inject a snippet of JavaScript into the page for execution in the context of the currently selected frame. The executed script is assumed to be asynchronous and must signal that is done by invoking the provided callback, which is always provided as the final argument to the function. The value to this callback will be returned to the client.

2.  adding custom commands
    `browser.addCommand`
    If you want to extend the browser instance with your own set of commands, the browser method addCommand is here for you.

#### Webapp

Staticly served

-   Change datasource in manifest
-   switched to openui5 cdn

### Debugging

see launch.json

#### Android

start appium with flags

#### iOS

## Reuse

### Utils

-   screenshot

### UI5 Integration

To be able to use specific ui5 locators and selectors instead of unified webdriver specs this project adds custom defined selectors.
https://webdriver.io/docs/selectors.html#custom-selector-strategies

https://webdriver.io/docs/api/browser/execute.html

#### Maxims Proposal

Here are few references that I promised.
So the example how to add synchronization of test with a UI framework that I mentioned is here: https://github.com/SAP/ui5-webcomponents/blob/91ac4d6ab89e33af12940c4bc87484d1b2dfb864/packages/tools/components-package/wdio.js#L248
Please keep in mind this is only to show that you can do it with WDIO, you can’t directly use this code because colleagues use their own rendering and synchronization with it, this is not UI5 runtime !.

What I suggest is that you use the WDIO options to create custom commands like browser.ui5\$() that would first synchronize using the RecordReplay.waitForUI5 and then use control locators by using `RecordReplay.findDomElementForControlLocator()`. You can also have a custom command to get the control from the dom element, similarly to uiveri5 asControl().
This way you can use the UI5-prefixed commands to work with UI5 parts of the app and have most of the benefits of uiveri5. And for the non-ui5 part of the app you use the native-WDIO commans.

#### Use one of

Load it in the testfile
`const path = require('path'); require('./../wdio-ui5/selectors');`

Add selectors in a webdriver hook ... (not working ?)
