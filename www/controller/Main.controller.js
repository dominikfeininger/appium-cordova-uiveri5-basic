sap.ui.define([
    "test/Sample/controller/BaseController",
    "sap/m/MessageToast"
], (Controller, MessageToast) => {
    "use strict";

    return Controller.extend("test.Sample.controller.Main", {
        // (live) transpiling async functions to ES5 generators not yet doable in ui5-tooling ecosys :)
        /* async */ onInit() {

            fetch("https://latest-openui5.rikosjett.com/api/v1/latest?format=json")
                .then(response => response.json())
                .then(latestU5version => {
                    this.getModel('LatestUI5').setProperty("/latest", latestU5version.version);
                })
                .catch(err => console.error(err))

        },

        navFwd() {
            return this.getOwnerComponent().getRouter().navTo("RouteOther");
        },

        onPress(oEvent) {
            MessageToast.show(`${oEvent.getSource().getId()} pressed`);
        },
        onBoo(oEvent) {
            MessageToast.show(`👻`);

        }
    });
});
